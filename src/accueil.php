<?php
session_start();

require ('conn.php');

if(isset($_SESSION['guid']) AND $_SESSION['guid'] > 0)
{
    $getguid = intval($_SESSION['guid']);
    $verifuser = $connectiondb->prepare('SELECT * FROM accounts WHERE guid = ?');
    $verifuser->execute(array($getguid));
    $accountinfo = $verifuser->fetch();
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>accueil</title>
</head>
<body>
    <h2>Bienvenue <?php echo $accountinfo['account']; ?> <h2>
    <?php
    if(isset($_SESSION['guid']) AND $accountinfo['guid'] == $_SESSION['guid'])
    {
        ?>
        <a href="changemdp.php">changer de mdp</a>
        <br>
        <br>
        <a href="logout.php">Déconnexion</a>
        <?php
    }
    ?>

</body>
</html>
